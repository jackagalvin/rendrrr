use nalgebra::Vector3;

fn main() {
    const IMAGE_WIDTH: u32 = 256;
    const IMAGE_HEIGHT: u32 = 256;

    println!("P3");
    println!("{} {}", IMAGE_WIDTH, IMAGE_HEIGHT);
    println!("255");

    for row in (0..IMAGE_HEIGHT).rev(){
        eprint!("\rScanlines Remaining Left: {} ", row);
        for column in 0..IMAGE_WIDTH{
            let colour = Vector3::new(column as f32 / (IMAGE_WIDTH - 1) as f32, row as f32/ (IMAGE_HEIGHT - 1) as f32, 0.25 as f32);
            let ir = (colour[0] * 255.999) as u32;
            let ig = (colour[1] * 255.999) as u32;
            let ib = (colour[2] * 255.999) as u32;

            println!("{} {} {}", ir, ig, ib);
        }
    }
}

